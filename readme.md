# Command Line usage : 
## Store in database
> php index.php

## Run in test mode
> php index.php tests

# Data location:
> cloudEmployee/csv_data/stock.csv

# Additional Query Location
> cloudEmployee/database/tblProductData_add_price_and_stock.sql

# Database Configuration Settings
> /src/model/baseModel.php
> This should be moved on a separate environment file