<?php

include_once('./csvParser.php');

class ImportProduct extends CsvParser
{
    protected $headersInDb = [
        'strProductCode',
        'strProductName',
        'strProductDesc',
        'stock',
        'price',
        'dtmDiscontinued'
    ];

    public function validate()
    {
        parent::validate();
        // Call other custom validation unique to importProduct here
        // ie.
        $this->validateHeaders();
    }

    public function validateHeaders()
    {
        // implement header checks of the csv file is correct
        // ie. missing header information, wrong header name, etc.
    }

    public function setDbHeadersAsKeys()
    {
        $tempCsvBody = [];
        $headersCount = count($this->headersInDb);
        
        foreach($this->csvBody as $entry) {
            $entryCount = count($entry);
            if($entryCount == $headersCount) {
                $tempCsvBody[] = array_combine($this->headersInDb, $entry);
            } else {
                $tempEntry = [];
                foreach($entry as $key => $entryData) {
                    $tempEntry[$this->headersInDb[$key]] = $entryData;
                }
                $tempCsvBody[] = $tempEntry;
            }
            
        }

        $this->csvBody = $tempCsvBody;
    }   
}