<?php

class CsvParser
{
    protected $csvMimeTypes = [ 
        'text/csv',
        'text/plain',
        'application/csv',
        'text/comma-separated-values',
        'application/excel',
        'application/vnd.ms-excel',
        'application/vnd.msexcel',
        'text/anytext',
        'application/octet-stream',
        'application/txt',
    ];

    protected $csvFile;
    protected $csvHeader;
    protected $csvBody;
    protected $errors = [];

    public function __construct($csvFile, $withHeader = true)
    {
        $this->csvFile = $csvFile;
        $csvArray = array_map('str_getcsv', file($csvFile));
        if($withHeader) {
            $this->csvHeader = array_shift($csvArray); // Get the first entry on the array and set as header
            $this->csvBody = $csvArray;
        } else {
            $this->csvBody = $csvArray;
        }
    }

    // 
    public function validate()
    {
        $this->isCsv();
    }

    protected function isCsv()
    {
        $finfo = finfo_open( FILEINFO_MIME_TYPE );
        $mimeType = finfo_file( $finfo, $this->csvFile  );

        if(!in_array( $mimeType, $this->csvMimeTypes )) {
            $this->errors[] = 'Invalid CSV Mime Type';
        }
    }

    public function getBody()
    {
        return $this->csvBody;
    }

    public function getHeader()
    {
        return $this->csvHeader;
    }

    public function hasErrors()
    {
        return !empty($this->errors);
    }
}