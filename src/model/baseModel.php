 <?php

class BaseModel extends mysqli
{
    private $servername = "mariadb";
    private $name = "root";
    private $password = "password";
    private $dbname = "wren";

    public function __construct()
    {
        parent::__construct(
            $this->servername,
            $this->name,
            $this->password,
            $this->dbname
        );

        $this->checkConnection();
    }

    public function checkConnection()
    {
        /* check connection */
        if (mysqli_connect_errno()) {
            throw new Exception("Connect failed: %s\n", mysqli_connect_error());
        }
    }
}
