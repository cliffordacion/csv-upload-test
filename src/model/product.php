<?php

include_once('./baseModel.php');

class Product extends BaseModel
{
    const PRICE_IMPORT_MAX_THRESHOLD = 1000;
    const PRICE_IMPORT_MIN_THRESHOLD = 5;
    const STOCK_IMPORT_MIN_THRESHOLD = 10;


    protected $tableName = 'tblProductData';

    protected $primaryKey = 'intProductDataId';

    protected $isNull = true;

    public $attributes = [
        'intProductDataId',
        'strProductName',
        'strProductDesc',
        'strProductCode',
        'stock',
        'price',
        'dtmAdded',
        'dtmDiscontinued'
    ];

    public $currentDate;

    public function __construct($arg = [])
    {
        parent::__construct();
        $this->currentDate = new DateTime;

        if (is_array($arg)) {
            foreach($this->attributes as $attribute) {
                $this->{$attribute} = $arg[$attribute] ?? null;
            }
            $this->isNull = false;
        } else {
            foreach($this->attributes as $attribute) {
                $this->{$attribute} = null;
            }
            $this->isNull = true;
        }

        $this->validate();
    }

    public function __destruct()
    {
        $this->close();
    }

    public function validate()
    {
        if(!is_numeric($this->stock) && !empty($this->stock)) {
            throw new Exception('Stock entered is not numeric : ' . $this->stock);
        }
        if(!is_numeric($this->price) && !empty($this->price)) {
            throw new Exception('Price entered is not numeric : ' . $this->price);
        }
    }

    public function save()
    {
        $this->setStmTimestamp();

        if($this->{$this->primaryKey} === null) {
            $this->create();
        } else {
            $this->update();
        }
    }

    public function create()
    {
        $productData = array_diff($this->attributes, [$this->primaryKey]);
        $attributeString = implode(', ', $productData);

        $query = "INSERT INTO " . $this->tableName . " (" . $attributeString . ") VALUES (?,?,?,?,?,?,?)";
        $stmt = $this->prepare($query);
        $stmt->bind_param('sssidss', 
            $this->strProductName,
            $this->strProductDesc,
            $this->strProductCode,
            $this->stock,
            $this->price,
            $this->dtmAdded,
            $this->dtmDiscontinued
        );
        $stmt->execute();
        // Throw error
        if($stmt->error) {
            throw new Exception($stmt->error, 400);
        }
    }

    public function update()
    {
        // implement if there is a need to update
    }

    protected function setStmTimestamp()
    {
        $this->stmTimestamp = $this->currentDate->format('Y-m-d H:i:s');
    }


    public function isNull()
    {
        return $this->isNull;
    }

    public function isForImport()
    {
        if(  
            $this->isPriceOverThreshold() ||
            $this->isPriceBelowThreshold() ||
            $this->isStockBelowThreshold()
        ) {
            return false;
        }
        return true;
    }

    protected function isPriceOverThreshold()
    {
        return $this->price > self::PRICE_IMPORT_MAX_THRESHOLD;
    }

    protected function isPriceBelowThreshold()
    {
        return $this->price < self::PRICE_IMPORT_MIN_THRESHOLD;
    }

    protected function isStockBelowThreshold()
    {
        return $this->stock < self::STOCK_IMPORT_MIN_THRESHOLD;
    }
}
