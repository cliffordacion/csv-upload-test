<?php
include_once('../../model/product.php');


class ProductService
{
    protected $errors = [];
    protected $testMode = false;

    public function importCsv($csvFile, $testMode = false)
    {
        $this->testMode = $testMode;
        $importProducts = new ImportProduct($csvFile);
        $importProducts->validate();
        if(!$importProducts->hasErrors()) {
            $importProducts->setDbHeadersAsKeys();
            $productData = $importProducts->getBody();
        }

        $currentDate = new DateTime;

        foreach($productData as $productEntry) {
            if(strtoupper($productEntry['dtmDiscontinued']) === "YES") {
                $productEntry['dtmDiscontinued'] = $currentDate->format('Y-m-d H:i:s');
            } else {
                $productEntry['dtmDiscontinued'] = null;
            }
            $productEntry['dtmAdded'] = $currentDate->format('Y-m-d H:i:s');
            try {
                $productObj = new Product($productEntry);
                if($productObj->isForImport()) {
                    if($testMode === false) {
                        $productObj->save();
                    }
                }
            } catch(Exception $e) {
                $this->errors[] = [
                    "product_code" => $productEntry['strProductCode'],
                    "error_message" => $e->getMessage()
                ];
            }
        }

        $this->generateConsoleReport();
    }

    public function generateConsoleReport()
    {
        echo "\n\nInsertion Error Report \n\n";
        foreach($this->errors as $errors) {
            echo '    ' . $errors["product_code"] . ' : '. $errors["error_message"] . "\n";
        }
        echo "\n\nEnd of Report \n\n";
        if($this->testMode) {
            echo "*** Running in Test Mode. No Data inserted***\n\n";
        }
    }

}