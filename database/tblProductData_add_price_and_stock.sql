ALTER TABLE `tblProductData` 
ADD `stock` INT NULL AFTER `strProductCode`, 
ADD `price` FLOAT NULL AFTER `stock`;