<?php
// Enable error Display on PHP
error_reporting(E_ERROR | E_PARSE);
ini_set('display_errors', 1);


include_once('./src/model/baseModel.php');
include_once('./src/model/product.php');
include_once('./src/service/productService.php');
include_once('./src/utilities/csvParser.php');
include_once('./src/utilities/importProduct.php');

$testMode = false;
if (isset($argc)) {
    if($argv[1] === 'test') {
        $testMode = true;
    }
}
else {
    echo "argc and argv disabled\n";
}

$productService = new ProductService();
$productService->importCsv(__DIR__ . '/csv_data/stock.csv', $testMode);